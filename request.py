import requests
import sys

f = open("result.txt", "a")

url = 'https://www.agcom.it/numerazionicallcenter?p_p_id=ricercaelencopubblico_WAR_elencopubblicoportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=single-column&p_p_col_pos=1&p_p_col_count=2&_ricercaelencopubblico_WAR_elencopubblicoportlet_javax.portlet.action=searchInElenco'

if len(sys.argv) < 2:
    print "Usage: python request.py Phone_Number_to_check"
    sys.exit(1)
number = sys.argv[1]

myobj = {'numerotelefono':number}
#myobj = {'numerotelefono':'0289731873'}

x = requests.post(url, data = myobj)

result = x.text
result = result.encode('utf-8')
if("trovati: " in result):
    result = result.split("trovati:", 1)[1]
    result = result[:5]
    result = filter(lambda x: x.isdigit(), result)
    print "Il numero risulta nel registro di AGCOM con " + result + " voci associate ad esso"
    f.write(result)
else:
    print "Il numero sembra essere pulito"
f.close()
